<?php
/**
 * @package M3_WEB_BROADCASTER
 * @version 1.0
 */
/*
Plugin Name: M3 Web Broadcaster
Plugin URI: https://bitbucket.org/m3mediasoftware/m3_web_broadcaster
Description: Wordpress plugin to send audio and video to a rtmp server (Wowza in first case).
Author: Hugo Mercado
Version: 1.0
Author URI: https://bitbucket.org/hmercado
*/
